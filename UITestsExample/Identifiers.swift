import Foundation

enum Identifiers {
  enum ViewController {
    static let id = "ViewController"
    static let textField = "TextField"
    static let submitBtn = "Submit Button"
    static let actionItem = "ActionItem"

    enum Alert {
      static let close = "Close"
    }

    enum ActionSheet {
      static let close = "Close"
      static let clear = "Clear Text Field"
    }
  }

  enum SummaryViewController {
    static let id = "SummaryViewController"
    static let info = "InfoLbl"
  }
}