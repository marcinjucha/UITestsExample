//
//  SummaryViewController.swift
//  UITestsExample
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 Accenture. All rights reserved.
//

import UIKit

class SummaryViewController: UIViewController {

  @IBOutlet weak var infoLbl: UILabel! {
    didSet {
      infoLbl.accessibilityIdentifier = "InfoLbl"
      infoLbl.text = text
    }
  }

  var text: String?

  override func viewDidLoad() {
    super.viewDidLoad()

    view.accessibilityIdentifier = SummaryViewController.reuseIdentifier
  }
}
