import UIKit

class ViewController: UIViewController {

  @IBOutlet weak var textField: UITextField! {
    didSet {
      textField.accessibilityIdentifier = "TextField"
      textField.delegate = self

      let attributed = NSAttributedString(string: "Enter the text ...",
                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
      textField.attributedPlaceholder = attributed
    }
  }
  @IBOutlet weak var submitBtn: UIButton! {
    didSet {
      submitBtn.accessibilityIdentifier = "Submit Button"
    }
  }
  @IBOutlet weak var actionItem: UIBarButtonItem! {
    didSet {
      actionItem.accessibilityIdentifier = "ActionItem"
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    view.accessibilityIdentifier = ViewController.reuseIdentifier

    let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:)))
    tap.cancelsTouchesInView = false
    view.addGestureRecognizer(tap)
  }

  @IBAction func onSubmit(_ sender: Any) {
    if textField.text?.isEmpty ?? true {
      let alert = UIAlertController(title: "Empty Text Field", message: "Enter the text", preferredStyle: .alert)

      alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { _ in
        alert.dismiss(animated: true, completion: nil)
      }))
      present(alert, animated: true, completion: nil)
    } else {
      performSegue(SummaryViewController.self, sender: textField.text!)
    }
  }

  @IBAction func onAction(_ sender: Any) {
    let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    alert.addAction(UIAlertAction(title: "Clear Text Field", style: .default, handler: { _ in
      self.textField.text = nil
      alert.dismiss(animated: true, completion: nil)
    }))

    alert.addAction(UIAlertAction(title: "Close", style: .destructive, handler: { _ in
      alert.dismiss(animated: true, completion: nil)
    }))
    present(alert, animated: true, completion: nil)
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let text = sender as? String,
       let vc = segue.destination as? SummaryViewController {
      vc.text = text
    }
  }
}

extension ViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return false
  }
}
