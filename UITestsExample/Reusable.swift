//
//  Reusable.swift
//  UITestsExample
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import UIKit

protocol Reuseable {}

extension Reuseable {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
}

extension UIViewController: Reuseable {}

extension UIViewController {
  func performSegue<T: Reuseable>(_: T.Type = T.self, sender: Any? = nil) {
    performSegue(withIdentifier: T.reuseIdentifier, sender: sender)
  }
}
