//
//  UITestsExampleUITests.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import XCTest

class UITestsExampleUITests: XCTestCase {

    var main: ViewControllerPage!
    var keyboard: AlphanumericKeyboard!
    var alert: ViewControllerAlertPage!
    var actionSheet: ViewControllerActionSheetPage!
    var summary: SummaryViewControllerPage!

    var app: XCUIApplication!

    override func setUp() {
        app = XCUIApplication()
        app.launch()

        main = ViewControllerPage()
        keyboard = AlphanumericKeyboard()
        alert = ViewControllerAlertPage()
        actionSheet = ViewControllerActionSheetPage()
        summary = SummaryViewControllerPage()
    }

    override func tearDown() {
        main = nil
        keyboard = nil
        alert = nil
        actionSheet = nil
        summary = nil

        app = nil
    }

    func testAlertShouldAppearWhenClickSubmitButtonAndTextFieldIsEmpty() {

        main
            .assert(\.screen, [.exists])

            .tapAndRedirect(\.submitButton, redirectTo: alert)
            .assert(\.screen, [.exists])
            .assert(\.message, [.exists, .containsLabel("Enter thetext")])
            .assert(\.closeButton, [.exists])

            .tapAndRedirect(\.closeButton, redirectTo: main)
            .assertPage(alert, element: \.screen, [.notExists])
    }

    func testActionSheetShouldAppearWhenClickOnActionItem() {
        main
            .assert(\.screen, [.exists])

            .tapAndRedirect(\.actionItem, redirectTo: actionSheet)
            .assert(\.screen, [.exists])

            .tapAndRedirect(\.cancelButton, redirectTo: main)
            .assertPage(alert, element: \.screen, [.notExists])
    }

    func testActionSheetClearTextFieldButtonShouldClearTextField() {
        main
            .assert(\.screen, [.exists])

            .typeAndReturn(text: "test", forTextField: \.textField)
            .assert(\.textField.screen, [.containsValue("test")])

            .tapAndRedirect(\.actionItem, redirectTo: actionSheet)
            .tapAndRedirect(\.clearTextFieldButton, redirectTo: main)

            .assert(\.textField.screen, [.containsValue("")])
    }

}
