//
//  KeyboardPage.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import XCTest

enum KeyboardType {
  case alphanumeric

  var page: KeyboardPage {
    switch self {
    case .alphanumeric: return AlphanumericKeyboard()
    }
  }
}

protocol KeyboardPage: Page {
  var returnKey: XCUIElement { get }
}

extension KeyboardPage {
  @discardableResult
  func tapReturn(_ file: StaticString = #file, _ line: UInt = #line) -> KeyboardPage {
    assert(\.returnKey, [.isHittable], file, line)
    tap(\.returnKey)

    return self
  }

  @discardableResult
  func tap(key: String, _ file: StaticString = #file, _ line: UInt = #line) -> Self {
    assert(\.returnKey, [.isHittable], file, line)
    app.keys[key].firstMatch.tap()
    return self
  }

  @discardableResult
  func type(value: String, _ file: StaticString = #file, _ line: UInt = #line) -> Self {
    value.forEach { tap(key: String($0), file, line) }
    return self
  }
}

class AlphanumericKeyboard: KeyboardPage {
  var id: String { "a" }

  var returnKey: XCUIElement { app.keyboards.buttons["Return"].firstMatch }
}

class NumericKeyboard: KeyboardPage {
  var id: String { "1" }

  var returnKey: XCUIElement { app }
}
