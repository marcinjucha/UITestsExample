//
//  AlertPage.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import XCTest

class AlertPage: Page {
  var screen: XCUIElement {
    return app.alerts.firstMatch
  }

  func has(text: String) -> Bool {
    return screen.staticTexts[text].firstMatch.exists
  }

  func has(button: String) -> Bool {
    return screen.buttons[button].firstMatch.exists
  }

  func tap(button: String) -> AlertPage {
    screen.buttons[button].firstMatch.tap()

    return self
  }

  required init() {}
}
