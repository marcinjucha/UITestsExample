//
//  ViewControllerActionSheetPage.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import XCTest

class ViewControllerActionSheetPage: ActionSheetPage {

  let ids = Identifiers.ViewController.ActionSheet.self

  var cancelButton: XCUIElement { screen.buttons[ids.close].firstMatch }

  var clearTextFieldButton: XCUIElement { screen.buttons[ids.clear].firstMatch }
}
