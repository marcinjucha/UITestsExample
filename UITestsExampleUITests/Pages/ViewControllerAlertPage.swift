//
//  ViewControllerAlertPage.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import XCTest

class ViewControllerAlertPage: AlertPage {
  enum Strings {
    static let title = "Empty Text Field"
    static let message = "Enter the text"

    static let closeButton = "Close"
  }

  var title: XCUIElement { screen.staticTexts[Strings.title].firstMatch }

  var message: XCUIElement { screen.staticTexts[Strings.message].firstMatch }

  var closeButton: XCUIElement { screen.buttons[Strings.closeButton].firstMatch }
}
