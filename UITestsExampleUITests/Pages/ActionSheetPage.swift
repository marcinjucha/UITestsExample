//
//  ActionSheetPage.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import XCTest

class ActionSheetPage: Page {
  var screen: XCUIElement { app.sheets.firstMatch }

  var buttons: XCUIElementQuery { screen.buttons }
}
