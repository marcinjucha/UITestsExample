//
//  Page.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import Foundation
import XCTest

private let defaultTimeout = 30.0

protocol Page {
  var id: String { get }
  var screen: XCUIElement { get }
}

extension Page {

  var id: String { "" }

  var app: XCUIApplication { XCUIApplication() }

  var screen: XCUIElement { app.descendants(matching: .any)[id].firstMatch }

  var backButton: XCUIElement { app.navigationBars.buttons.firstMatch }
}

extension Page {
  @discardableResult
  func type(text: String,
            forTextField keyPath: KeyPath<Self, TextFieldPage>,
            timeout: TimeInterval = defaultTimeout,
            _ file: StaticString = #file,
            _ line: UInt = #line) -> TextFieldPage {
    let textField = self[keyPath: keyPath]
    textField.type(value: text, file, line)

    return textField
  }

  @discardableResult
  func typeAndReturn(text: String,
                     forTextField keyPath: KeyPath<Self, TextFieldPage>,
                     timeout: TimeInterval = defaultTimeout,
                     _ file: StaticString = #file,
                     _ line: UInt = #line) -> Self {
    let textField = self[keyPath: keyPath]
    textField.type(value: text, file, line)
    textField.keyboard.tapReturn(file, line)
    assert(textField.keyboard.screen, [.notExists], timeout: timeout, file, line)

    return self
  }

}

extension Page {
  @discardableResult
  func tap(_ keyPath: KeyPath<Self, XCUIElement>,
           timeout: TimeInterval = defaultTimeout,
           _ file: StaticString = #file,
           _ line: UInt = #line) -> Self {
    let element = self[keyPath: keyPath]
    assert(keyPath, [.isHittable], timeout: timeout, file, line)
    element.tap()

    return self
  }

  @discardableResult
  func tapAndRedirect<T: Page>(_ keyPath: KeyPath<Self, XCUIElement>,
                               redirectTo page: T,
                               timeout: TimeInterval = defaultTimeout,
                               _ file: StaticString = #file,
                               _ line: UInt = #line) -> T {
    tap(keyPath, timeout: timeout, file, line)

    return page
  }
}

extension Page {
  @discardableResult
  func tapScreen(_ file: StaticString = #file,
                 _ line: UInt = #line) -> Self {
    app.tap()

    return self
  }
}

extension Page {
  @discardableResult
  func assert(_ keyPath: KeyPath<Self, XCUIElement>,
              _ predicates: [Predicate],
              timeout: TimeInterval = defaultTimeout,
              _ file: StaticString = #file,
              _ line: UInt = #line) -> Self {
    let element = self[keyPath: keyPath]
    assert(element, predicates, timeout: timeout, file, line)

    return self
  }

  @discardableResult
  func assertPage<T: Page>(_ page: T,
                           element keyPath: KeyPath<T, XCUIElement>,
                           _ predicates: [Predicate],
                           timeout: TimeInterval = defaultTimeout,
                           _ file: StaticString = #file,
                           _ line: UInt = #line) -> Self {
    page.assert(keyPath, predicates, timeout: timeout, file, line)

    return self
  }

  @discardableResult
  func assert(_ element: XCUIElement,
              _ predicates: [Predicate],
              timeout: TimeInterval = defaultTimeout,
              _ file: StaticString = #file,
              _ line: UInt = #line) -> Self {
    let expectation = XCTNSPredicateExpectation(
      predicate: NSPredicate(format: predicates.map { $0.format }.joined(separator: " AND ")),
      object: element
    )
    guard XCTWaiter.wait(for: [expectation], timeout: timeout) == .completed else {
      XCTFail("[\(self)] Element \(element.description) did not fulfill expectation: \(predicates.map { $0.format })",
              file: file,
              line: line)
      return self
    }

    return self
  }
}
