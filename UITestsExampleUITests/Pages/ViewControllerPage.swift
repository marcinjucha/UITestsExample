import XCTest

class ViewControllerPage: Page {
  let identifiers = Identifiers.ViewController.self

  var id: String { identifiers.id }

  lazy var textField = TextFieldPage(id: identifiers.textField, keyboardType: .alphanumeric)

  var submitButton: XCUIElement { app.buttons[identifiers.submitBtn].firstMatch }

  var actionItem: XCUIElement { app.navigationBars.buttons[identifiers.actionItem].firstMatch }

}
