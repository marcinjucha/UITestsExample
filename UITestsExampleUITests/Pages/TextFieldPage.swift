//
//  TextFieldPage.swift
//  UITestsExampleUITests
//
//  Created by Marcin Jucha on 08.10.2018.
//  Copyright © 2018 mjucha. All rights reserved.
//

import XCTest

class TextFieldPage: Page {
  var element: XCUIElement!

  let keyboard: KeyboardPage

  var screen: XCUIElement { element }

  var text: String { element.value as? String ?? "" }

  var placeholder: String { element.placeholderValue ?? "" }

  @discardableResult
  func tap(_ file: StaticString = #file,
           _ line: UInt = #line) -> TextFieldPage {
    assert(\.screen, [.isHittable], file, line)
    screen.tap()
    return self
  }

  @discardableResult
  func type(value: String,
            _ file: StaticString = #file,
            _ line: UInt = #line) -> TextFieldPage {
    tap(file, line)
    screen.typeText(value)

    return self
  }

  init(id: String, keyboardType: KeyboardType) {
    keyboard = keyboardType.page
    element = app.textFields[id].firstMatch
  }
}

class SecuredTextField: TextFieldPage {
  override init(id: String, keyboardType: KeyboardType) {
    super.init(id: id, keyboardType: keyboardType)
    element = app.secureTextFields[id].firstMatch
  }
}
